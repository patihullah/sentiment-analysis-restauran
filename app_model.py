from keras.models import load_model
from core.preprocessing import Preprocessing
from gensim.models import Word2Vec
import string

if __name__ == '__main__':
    # dimensi data (2400, 55, 50)

    # load model Word2Vec
    print("Load Model Word2Vec ... ")
    filemodel = 'data_model/w2v/W2vec_wiki_tambah_50_id.bin'
    model_w2v = Word2Vec.load(filemodel)

    # load Model GRU
    print("Load Model GRU ...")
    model_best = load_model('data_model/model/GRU_best_kfold.h5')
    model_best.summary()

    # instansiasi class
    pro = Preprocessing()
    translator = str.maketrans('', '', string.punctuation)

    text = "pelayanannya tidak memuaskan, masa saya pesan nasi goreng datangnya 1 jam"
    print("Data yang di coba: {}".format(text))
    # preprocessing
    text = pro.removepunch(text, translator)
    # text = pro.stopword(text)
    text = [text]

    # panjang maksimum teks = 55
    text = pro.transfor_term_3d(text, 55, model_w2v)

    prediksi = model_best.predict(text)
    if prediksi >= 0.5:
        print("Sentiment: Positive")
    else:
        print("Sentiment: Negative")
    print("Confidance: %1.1f" % prediksi)
