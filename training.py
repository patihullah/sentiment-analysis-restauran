import csv
import string
import numpy as np
from core.preprocessing import Preprocessing
from gensim.models import Word2Vec
from core.toolsmodel import Toolsmodel
from sklearn.model_selection import train_test_split

def ekstraksi_data(filein):
    data = []
    label = []
    with open(filein, 'r') as tsv_in:
        tsv_reader = csv.reader(tsv_in, delimiter='\t')
        for col in tsv_reader:
            data.append(col[0])
            label.append(col[1])

    return data, label


if __name__ == '__main__':
    # Load model Word2vec
    filemodel = 'data_model/w2v/W2vec_wiki_tambah_50_id.bin'
    model_w2v = Word2Vec.load(filemodel)

    prop = Preprocessing()
    tools = Toolsmodel()

    # Mengambil data training dan data testing yang akan digunakan
    file_data_train = 'data source/train_data_restaurant.tsv'
    file_data_test = 'data source/test_data_restaurant.tsv'

    data_train, label_train = ekstraksi_data(file_data_train)
    data_test, label_test = ekstraksi_data(file_data_test)

    print("jumlah data training: {}".format(len(data_train)))
    print("jumlah data test: {}".format(len(data_test)))

    translator = str.maketrans('', '', string.punctuation)
    # remove punctuation in data
    data_train = [prop.removepunch(text, translator) for text in data_train]
    data_test = [prop.removepunch(text, translator) for text in data_test]

    # Stopword
    data_train = [prop.stopword(teks) for teks in data_train]
    data_test = [prop.stopword(teks) for teks in data_test]

    print(data_train[0])

    # Karena data class imbalance, dan dapat mengakibatkan model lebih cenderung pada class tertentu
    # maka kita menghandle data imbalance class tersebut dengan menaikkan jumlah kelas hingga sama dengan
    # jumlah mayoritas class. cara ini disebut dengan UP-sampling

    # up-sample minority class
    data_all_upsampling = tools.up_sampling(data_train, label_train)

    print("Jumlah Data training: {}".format(len(data_all_upsampling)))

    train_data = []
    train_label = []
    for data in data_all_upsampling:
        train_data.append(data[0])
        train_label.append(data[1])

    # split data dan label
    X_train, x_test, Y_train, y_test = train_test_split(train_data, train_label, test_size=0.20, random_state=1000)

    p = 0
    ne = 0
    for n in Y_train:
        if n == "positive":
            p += 1
        elif n == "negative":
            ne += 1

    print("Total Positif: {}, Negativ: {}".format(p, ne))
    print("Total seluruh data training: {}".format(p + ne))

    # fitur transform
    maxtw = max([len(s.split()) for s in data_train])
    print(maxtw)
    x_data_train = np.asarray(prop.transfor_term_3d(X_train, maxtw, model_w2v))
    y_label_train = np.asarray(prop.embed_label(Y_train))
    # data validasi
    x_data_valid = np.asarray(prop.transfor_term_3d(x_test, maxtw, model_w2v))
    y_label_valid = np.asarray(prop.embed_label(y_test))
    # Data Testing
    x_data_testing = np.asarray(prop.transfor_term_3d(data_test, maxtw, model_w2v))
    y_label_testing = np.asarray(prop.embed_label(label_test))

    print("Ukuran data training: {}, label: {} dan validasi: {}, label: {}".format(x_data_train.shape,
                                                                                   y_label_train.shape,
                                                                                   x_data_valid.shape,
                                                                                   y_label_valid.shape))
    print("Ukuran data testing: {}, label: {}".format(x_data_testing.shape, y_label_testing.shape))

    print(x_data_train[0])

    # # membuat Model
    # model = tools.createmodel(x_data_train)
    # # Training model
    # history = model.fit(x_data_train, y_label_train, epochs=20, batch_size=128, validation_data=(x_data_valid, y_label_valid))
    #
    # # Evaluasi model
    # print("Evaluasi Model :")
    # score, acc = model.evaluate(x_data_testing, y_label_testing, batch_size=128)
    #
    # print('Score: %1.4f' % score)
    # print('Accuracy: %1.4f' % acc)
    #
    # tools.plot_history(history)

    # Menggunakan KFoldStatifield. untuk mengetahui model terbaik dari data
    tools.foldstatifield(train_data, train_label, data_valid=x_data_testing, label_valid=y_label_testing, maxnum=maxtw, m2v=model_w2v)