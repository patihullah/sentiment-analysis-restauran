from gensim.models import Word2Vec
from gensim.models.word2vec import LineSentence
import multiprocessing
import logging
import os.path
import sys


def trainModel(filein, output):
    params = {'size': 50, 'window': 5, 'min_count': 5, 'workers': max(1, multiprocessing.cpu_count() - 1),
              'sample': 1E-3}

    md = Word2Vec(LineSentence(filein), **params)
    md.init_sims(replace=True)
    md.save(output)


def load_model(filemodel):
    return Word2Vec.load(filemodel)
    #   return word2vec.KeyedVectors.load_word2vec_format(filemodel, binary=True)


def weigth_word(kata, mod):
    wordd = []
    try:
        #   hasil = model.most_similar(w, topn=6)
        for term in kata:
            if term in mod.wv.vocab:
                wordd.append(mod.wv.get_vector(term))
            # else:
            #   print("kata", term)
        #   print(wordd[0])
        return wordd
    except KeyError as e:
        print(e)


if __name__ == '__main__':
    program = os.path.basename(sys.argv[0])
    logger = logging.getLogger(program)

    logging.basicConfig(format='%(asctime)s: %(levelname)s: %(message)s ')
    logging.root.setLevel(level=logging.INFO)
    logging.info("running %s" % ' '.join(sys.argv))

    fileinput = '../data source/wiki.id.Case.text'
    fileoutput = '../data_model/w2v/W2vec_wiki_tambah_50_id.bin'

    trainModel(fileinput, fileoutput)
