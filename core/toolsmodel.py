import numpy as np
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import GRU
from keras.layers import Dropout
from sklearn.model_selection import StratifiedKFold
import matplotlib.pyplot as plt
from core.preprocessing import Preprocessing
from sklearn.utils import resample


class Toolsmodel:
    def __init__(self):
        plt.style.use('ggplot')

    @staticmethod
    def plot_history(history):
        acc = history.history['accuracy']
        val_acc = history.history['val_accuracy']
        loss = history.history['loss']
        val_loss = history.history['val_loss']
        x = range(1, len(acc) + 1)

        plt.figure(figsize=(12, 5))
        plt.subplot(1, 2, 1)
        plt.plot(x, acc, 'b', label='Training acc')
        plt.plot(x, val_acc, 'r', label='Validation acc')
        plt.title('Training and validation accuracy')
        plt.legend()
        plt.subplot(1, 2, 2)
        plt.plot(x, loss, 'b', label='Training loss')
        plt.plot(x, val_loss, 'r', label='Validation loss')
        plt.title('Training and validation loss')
        plt.legend()

        plt.show()

    def foldstatifield(self, data, label, fold=10, data_valid=[], label_valid=[], maxnum=0, epoch=20, m2v=""):
        self.pr = Preprocessing()
        data_x = np.asarray(data)
        label_x = np.asarray(label)
        skf = StratifiedKFold(n_splits=fold)
        modelbaik = 0

        for train_index, test_index in skf.split(data_x, label_x):
            X_train, X_test = data_x[train_index], data_x[test_index]
            y_train, y_test = label_x[train_index], label_x[test_index]

            X_train = np.asarray(self.pr.transfor_term_3d(X_train, maxnum, m2v))
            y_train = np.asarray(self.pr.embed_label(y_train))

            X_test = np.asarray(self.pr.transfor_term_3d(X_test, maxnum, m2v))
            y_test = np.asarray(self.pr.embed_label(y_test))

            model = self.createmodel(X_train)
            history = model.fit(X_train, y_train, epochs=epoch, batch_size=128, validation_data=(X_test, y_test))

            score, acc = model.evaluate(data_valid, label_valid, batch_size=128)

            print('Score: %1.4f' % score)
            print('Accuracy: %1.4f' % acc)
            if acc > modelbaik:
                print("save model")
                model.save('data_model/model/GRU_best_kfold_new.h5')
                modelbaik = acc
                self.plot_history(history)

            print('\n')

    @staticmethod
    def createmodel(x_train):
        model = Sequential()
        model.add(GRU(50, input_shape=(x_train.shape[1], x_train.shape[2])))
        # model.add(LSTM(int(x_train.shape[1]*1.5), input_shape=(x_train.shape[1], x_train.shape[2])))
        # model.add(Dense(512, activation='relu'))
        # model.add(Dropout(0.2))
        model.add(Dense(1, activation='sigmoid'))
        # model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        model.compile(loss='mse', optimizer='adam', metrics=['accuracy'])
        print(model.summary())

        # history = model.fit(x_train, y_train, epochs=18, batch_size=128,
        #                     validation_data=(x_test, y_test))
        return model

    @staticmethod
    def up_sampling(data, label):
        data_minoritas = []
        data_mayoritas = []
        for index, dat in enumerate(label):
            if dat == "positive":
                data_mayoritas.append([data[index], label[index]])
            else:
                data_minoritas.append([data[index], label[index]])

        print("Jumlah class minorias: {}, Jumlah class mayoritas: {}".format(len(data_minoritas), len(data_mayoritas)))
        data_minoritas_upsample = resample(data_minoritas, replace=True, n_samples=int(len(data_mayoritas)),
                                           random_state=1000)
        print("Jumlah data class minoritas setelah upsampling: {}".format(len(data_minoritas_upsample)))
        # Menggabungkan data minoritas upsample dan mayoritas
        data_mayoritas.extend(data_minoritas_upsample)

        return data_mayoritas