import re
import numpy as np


class Preprocessing:
    def __init__(self):
        self.stoplist = self.data_stop()

    def removepunch(self, text, transl):
        clean_text = re.sub(r'(@\w*)|(#\w*)|\.(?!$-)|(\\w*)|(\?)', ' ', text)
        clean_text = " ".join(clean_text.split())
        return clean_text.translate(transl)

    @staticmethod
    def data_stop():
        with open("data source/stopword_list.txt", "r") as stl:
            da = stl.readlines()

        da = [d.rstrip('\n') for d in da]
        return da

    def stopword(self, dok):
        cln = [t for t in dok.split() if t not in self.stoplist]
        return ' '.join(cln)

    @staticmethod
    def word_weight(kata, model):
        wordd = []
        for term in kata:
            if term in model.wv.vocab:
                wordd.append(model.wv.get_vector(term))

        return wordd

    def transfor_term_3d(self, textclean, max_wv, model_w2v):
        x_data = np.zeros((len(textclean), max_wv, 50), dtype=np.float64)
        ttc = [[self.word_weight(tn.split(), model_w2v)] for tn in textclean]

        for key, val in enumerate(ttc):
            for vec in val:
                tr = max_wv - len(vec)
                for inde, wvc in enumerate(vec):
                    x_data[key, (tr + inde), :] = wvc

        return x_data

    def embed_label(self, label):
        label_em = []
        for l in label:
            if l == "positive":
                label_em.append(1.)
            else:
                label_em.append(0.)
        return label_em
